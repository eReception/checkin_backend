package ns.ereception.models;

public class CheckInBestaetigung extends MailNachricht {
	private String besucherAnrede;
	private String besucherNachname;
	
	public CheckInBestaetigung(String empfaenger, String besucherAnrede, String besucherNachname) {
		super(empfaenger);
		this.besucherAnrede = besucherAnrede;
		this.besucherNachname = besucherNachname;
	}

	public String getBesucherAnrede() {
		return besucherAnrede;
	}

	public void setBesucherAnrede(String besucherAnrede) {
		this.besucherAnrede = besucherAnrede;
	}

	public String getBesucherNachname() {
		return besucherNachname;
	}

	public void setBesucherNachname(String besucherNachname) {
		this.besucherNachname = besucherNachname;
	}



}
