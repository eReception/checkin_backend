/**
 * 
 */
package ns.ereception.models;

import javax.persistence.CascadeType;
/**
 * @author nabil
 *
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.Getter;
import lombok.AllArgsConstructor;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Besuch implements Serializable {
	
	
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id", unique=true)
	private long id;
	
	@Column(name ="code")
	private String code;
	
	@Column(name ="gastgeber")
	private String gastgeber;
	
	@Column(name ="check_In_Datum")
	private String checkInDatum;
	
	@Column(name ="check_In_Uhrzeit")
	private String checkInUhrzeit;
	
	@Column(name ="check_Out_Datum")
	private String checkOutDatum;
	
	@Column(name ="check_Out_Uhrzeit")
	private String checkOutUhrzeit;
	
	@OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "besucher_id")
    private Besucher besucher;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getGastgeber() {
		return gastgeber;
	}
	public void setGastgeber(String gastgeber) {
		this.gastgeber = gastgeber;
	}
	public Besucher getBesucher() {
		return besucher;
	}
	public void setBesucher(Besucher besucher) {
		this.besucher = besucher;
	}
	public long getId() {
		return id;
	}
	public String getCheckOutDatum() {
		return checkOutDatum;
	}
	public void setCheckOutDatum(String checkOutDatum) {
		this.checkOutDatum = checkOutDatum;
	}
	public String getCheckOutUhrzeit() {
		return checkOutUhrzeit;
	}
	public void setCheckOutUhrzeit(String checkOutUhrzeit) {
		this.checkOutUhrzeit = checkOutUhrzeit;
	}
	public String getCheckInDatum() {
		return checkInDatum;
	}
	public void setCheckInDatum(String checkInDatum) {
		this.checkInDatum = checkInDatum;
	}
	
	public String getCheckInUhrzeit() {
		return checkInUhrzeit;
	}
	public void setCheckInUhrzeit(String checkInUhrzeit) {
		this.checkInUhrzeit = checkInUhrzeit;
	}
}