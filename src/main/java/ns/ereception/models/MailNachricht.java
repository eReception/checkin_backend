package ns.ereception.models;

public class MailNachricht {
	private String empfaenger;
	
	public MailNachricht(String empfaenger) {
		this.empfaenger = empfaenger;

	}


	public String getEmpfaenger() {
		return empfaenger;
	}

	public void setEmpfaenger(String empfaenger) {
		this.empfaenger = empfaenger;
	}

	
}
