/**
 * 
 */
package ns.ereception.models;
/**
 * @author nabil
 *
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.Getter;
import lombok.AllArgsConstructor;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Mitarbeiter implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id", unique=true)
	private long id;
	
	@Column(name ="eMail")
	private String eMail;
	
	@Column(name ="vorname")
	private String vorname;
	
	@Column(name ="nachname")
	private String nachname;
	
	@Column(name ="anrede")
	private String anrede;
	
	@Column(name ="raumNr")
	private String raumNr;

	@Column(name ="rolle")
	private String rolle;

	public String getRaumNr() {
		return raumNr;
	}

	public void setRaumNr(String raumNr) {
		this.raumNr = raumNr;
	}

	public String getRolle() {
		return rolle;
	}

	public void setRolle(String rolle) {
		this.rolle = rolle;
	}

	public long getId() {
		return id;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getAnrede() {
		return anrede;
	}

	public void setAnrede(String anrede) {
		this.anrede = anrede;
	}

	
	
}