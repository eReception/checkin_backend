package ns.ereception.models;

public class MitarbeiterBenachrichtigung extends MailNachricht {

	private String besucherAnrede;
	private String besucherNachname;
	
	public MitarbeiterBenachrichtigung(String gastgeber, String besucherAnrede , String besucherNachname) {
		super(gastgeber);
		this.besucherAnrede = besucherAnrede;
		this.besucherNachname = besucherNachname;
	}

	public String getBesucherAnrede() {
		return besucherAnrede;
	}

	public void setBesucherAnrede(String besucherAnrede) {
		this.besucherAnrede = besucherAnrede;
	}

	public String getBesucherNachname() {
		return besucherNachname;
	}

	public void setBesucherNachname(String besucherNachname) {
		this.besucherNachname = besucherNachname;
	}



}
//text = "Sie haben einen Besuch von " + anrede +  " " + nachname " bekommen. Bitte kommen Sie zum Empfang, um ihn/sie abzuholen."
