package ns.ereception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EnableAutoConfiguration
@EnableJpaRepositories("ns.ereception.repository")
@SpringBootApplication
public class EReceptionFstApplication {


	public static void main(String[] args) {
		SpringApplication.run(EReceptionFstApplication.class, args);
	}

}
