package ns.ereception.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ns.ereception.services.NotfallService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class NotfallController {

	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private NotfallService notfallService;
	
	
	@RequestMapping("/schalter/sendeNotfallliste")
	public Boolean sendeNotfallListeAnBehoerden(){
		log.info("Notfallliste senden");
		return notfallService.sendeNotfallliste(); 
	}

}
