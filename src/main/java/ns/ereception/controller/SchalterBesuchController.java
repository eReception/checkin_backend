package ns.ereception.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ns.ereception.models.Besuch;
import ns.ereception.models.Besucher;
import ns.ereception.services.BesuchService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class SchalterBesuchController {
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private BesuchService besuchService;
	
	@RequestMapping("/schalter/zaehleBesucher")
	public long count() {
		log.info("Search total number of visits");
		return besuchService.getCount();
	}
	
	//@CrossOrigin(origins = "https://www.w3schools.com")
	@RequestMapping("/schalter/alleBesuche")
	public List<Besuch> getAllBesuchs(){
		log.info("Searching all besuchs");
		return besuchService.findAllBesuch();
	}
	
	@RequestMapping("/schalter/alleBesucher")
	public List<Besucher> getAllBesuchers(){
		log.info("Searching all visits");
		return besuchService.findAllBesucher();
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/schalter/checkInSchalter")
	public String addBesuch(@RequestBody Besuch besuch) {
		
		log.info("Creation/Updating Besuch - "+besuch.toString());
		return besuchService.addSchalterBesuch(besuch);
	}
	
	@RequestMapping("/schalter/findeBesuchMitId/{id}" )
	public Besuch findById(@PathVariable long id) {
		log.info("Searching besuch with ID - "+ id);
		return besuchService.findBesuchById(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/schalter/checkOut/{id}")
	public boolean deleteBesuch(@PathVariable long id) {
		return besuchService.deleteBesuch(id);
	}
	
	
}
