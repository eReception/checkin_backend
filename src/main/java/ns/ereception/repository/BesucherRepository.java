package ns.ereception.repository;

import org.springframework.data.repository.CrudRepository;
import ns.ereception.models.Besucher;

public interface BesucherRepository extends CrudRepository<Besucher, Long>{
}
