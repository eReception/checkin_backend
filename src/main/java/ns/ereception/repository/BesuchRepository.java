package ns.ereception.repository;

import org.springframework.data.repository.CrudRepository;
import ns.ereception.models.Besuch;

public interface BesuchRepository extends CrudRepository<Besuch, Long>{
}
