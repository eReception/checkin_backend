package ns.ereception.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ns.ereception.repository.BesuchRepository;
import ns.ereception.repository.BesucherRepository;
import ns.ereception.models.Besuch;
import ns.ereception.models.Besucher;
import ns.ereception.models.CheckInBestaetigung;
import ns.ereception.models.MitarbeiterBenachrichtigung;


@Service
public class BesuchService {
	
	@Autowired
	RestTemplate restTemplate;
	
	String serviceURL = "https://b71iy0d4zlgdtgxv-mailing-mailjs.cfapps.eu10.hana.ondemand.com";
	
	
	@Autowired
	private BesuchRepository besuchRepository;
	
	@Autowired
	private BesucherRepository besucherRepository;
	
	public long getCount() {
		long count = besuchRepository.count();
		return count;
	}
	
	public List<Besuch> findAllBesuch(){
		List<Besuch> besuch = new ArrayList<>();
		besuchRepository.findAll().forEach(besuch::add);
		return besuch;
	}
	
	public List<Besucher> findAllBesucher(){
		List<Besucher> besucher = new ArrayList<>();
		besucherRepository.findAll().forEach(besucher::add);
		return besucher;
	}
	
	public synchronized String addSchalterBesuch(Besuch besuch) {
	 try {
		 
		besuch.setCheckInUhrzeit(getCurrentTime());
		besuch.setCheckInDatum(getCurrentDate());
		besuchRepository.save(besuch);
		this.sendeBenachrichtigung(besuch);
		return "true";
	 	} 
	 catch (Exception e) {
		return e.toString();
		}
	}
	
	public synchronized String addOnlineBesuch(Besuch besuch) {
		 try {
			 
			besuchRepository.save(besuch);
			this.sendeBestaetigung(besuch.getBesucher());
			return "true";
		 	} 
		 catch (Exception e) {
			return e.toString();
			}
		}
	
	public Besuch findBesuchById(Long id) {
		Besuch besuch = besuchRepository.findById(id).orElse(null);
		return besuch;
	}
	
	public boolean deleteBesuch(long id) {
		Besuch besuch = besuchRepository.findById(id).orElse(null);
		if(besuch!=null) {
			besuchRepository.delete(besuch);
			return true;
		}
		return false;
	}
	
	private boolean sendeBestaetigung(Besucher besucher) {
		CheckInBestaetigung checkInBestaetigung = new CheckInBestaetigung (besucher.geteMail(), besucher.getAnrede(), besucher.getNachname());	
		restTemplate.postForObject(serviceURL + "/check_In_bestaitigung", checkInBestaetigung, String.class);

		return false;
	}
	
	private boolean sendeBenachrichtigung(Besuch besuch) {
		Besucher besucher = besuch.getBesucher();
		MitarbeiterBenachrichtigung mitarbeiterBenachrichtigung = new MitarbeiterBenachrichtigung (besuch.getGastgeber(), besucher.getAnrede(), besucher.getNachname());	
		restTemplate.postForObject(serviceURL + "/mitarbeiter_benachrichtigung", mitarbeiterBenachrichtigung, String.class);

		return false;
	}
	
	public String test() {
		return restTemplate.getForObject("https://ereception.cfapps.eu10.hana.ondemand.com/schalter/alleBesucher", String.class);

	}
	
	@Bean
	public RestTemplate rest() {
		return new RestTemplate();
	}
	
	public static String getCurrentTime() {
	    Calendar cal = Calendar.getInstance();
	    Date date = cal.getTime();
	    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	    String formattedDate = dateFormat.format(date);
	    return formattedDate;
	}
	
	public static String getCurrentDate() {
	    Calendar cal = Calendar.getInstance();
	    Date date = cal.getTime();
	    DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
	    String formattedDate = dateFormat.format(date);
	    return formattedDate;
	}
}
