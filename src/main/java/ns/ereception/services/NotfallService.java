package ns.ereception.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ns.ereception.repository.BesucherRepository;
import ns.ereception.models.Besucher;


@Service
public class NotfallService {

	@Autowired
	RestTemplate restTemplate;
	
	String serviceURL = "https://b71iy0d4zlgdtgxv-mailing-mailjs.cfapps.eu10.hana.ondemand.com";
	

	@Autowired
	private BesucherRepository besucherRepository;
	
	public List<Besucher> _findAllBesucher(){
		List<Besucher> besucher = new ArrayList<>();
		besucherRepository.findAll().forEach(besucher::add);
		return besucher;
	}
	
	public boolean sendeNotfallliste() {
		List<Besucher> besucherListe = this._findAllBesucher();
		restTemplate.postForObject(serviceURL + "/sendeNotfallliste", besucherListe, String.class);

		return true;
	}
	
	@Bean
	public RestTemplate restNotfall() {
		return new RestTemplate();
	}

}
